from setuptools import setup
setup(
     name='pre_commit_hooks',
     version='2.0.0',
     description='Some out-of-the-box hooks for pre-coomit.',
     author='Hugo Pacheco',
     packages = ['pre_commit_hooks'],
     install_requires=['python-dateutil', 'numpy', 'pandas', 'tabulate'],
     entry_points={
        'console_scripts': ['check-added-large-files = pre_commit_hooks.check_added_large_files:main',
                            'check-bteqs = pre_commit_hooks.check_bteqs:main'
        ]
    }
)