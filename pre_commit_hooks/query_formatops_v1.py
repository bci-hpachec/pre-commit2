#######################################################
# Primera version oficial de QueryOps
# Autor: Bruno Gomez
# Fecha: 20210520
# Version: 1.1 - 20210831
#######################################################

import glob
import datetime
import os
#from tqdm import tqdm
import json
from datetime import datetime, date
import numpy as np
import pandas as pd
import csv
from math import sqrt
#import seaborn as sns
import getpass
import shutil
import re
from collections import Counter

# lista_esquemas, actualizado al 20210322 mas correcciones
lista_esquemas = ["Edw_BciProd_Tb","prossi","Edm_DmJourney_Tb","ahirane","Edm_DmDotacion_Vw","PDCRSTG","mfernsa","tva_sor","Edm_CorePrspct_Vw","Edw_DmAnalic_Vw","tva_work",
"Edw_BciDatam_Tb","Edc_Journey_Tb","Edw_Event_Col_Tb","Edc_CnlDigital_Vw","SYSSPATIAL","acano","Edw_Epiphany_Tb","Edm_DmSegto_Tb","Edm_DmBdr_Tb","Mkt_Journey_Dig_Tb","conti","Edw_TareaSolicitud_Vw","Mkt_Crm_Cmp_Tb","usarmedw","BCIMACH","Mkt_Prevfraudes_Tb","Mkt_Calidad_Datos_Tb","Edw_Temp","Edm_DmJourney_Vw","BCIGPIO","EDW_Location_Tb","ARMCORE","SYSBAR","EDW_Va","jcparra","tva_archive","EDW_Vu","DG_TestLab_Tb","Edc_Ssff_Vw","mkt_JourneyBuilder_tb","Edw_Prospect_Vw","SQLJ","Edm_DmActCom_Tb","BCI_VU","fplaza","Edw_Event_Bel_Tb","Edw_Event_Dav_Tb","EDW_DmMkt_Tb","tva_profit","$NETVAULT_CATALOG","amendec","Edw_Cuadratura_Tb","demographics","Mkt_Gobierno_Datos_Tb","BCI_PRD","Edw_Event_Tdm_Tb","Mkt_Tmp_Iris_Tb","Edw_Governance_Tb","DS_ARMWORK","ARMVIEWS","rsamso","MKT_JOURNEY_TB","EDW_Event_Ctb_Tb","Edm_DmCto_Vw","DS_ARMCORE","fbissie","Edm_DmEmpresa_Tb","MKT_CRM_ANALYTICS_Vw","acruzad","dataqual","DS_ARMCUST","tva_views","BCIMES","EDW_Param_and_Others_Tb","Ebi_PlMtc_Vw","DS_EPUBCK","Edc_SurveyBuy_Tb","Edm_CorePrspct_Tb","PR_EPUSTG","BCI_MDS","DM_STAGING","Edm_Tmp_Dmr","BCIDAILY","Edw_DMTarjeta_Tb","Edm_DmSegto_Vw","bciperso","USRGTBCH","viewpoint","BCISEGRETAIL","Mkt_CnlDigital_Tb","DATAM","tva_results","Edc_Suc_Vw","Edc_Mach_Vw","EDW_DMBBP_TB","Edc_Suc_Tb","EDW_SEMLAY_VW","TDStats","PDCRTPCD","rpavefe","Ebi_PlByP_Vw","Edw_Event_Cmp_Tb","Edw_Calidad_Tb","BCIWSALE","Edw_Matrix_Vw","UsrCdbc","Edc_Ssff_Tb","Mkt_CRM_EMP_Tb","QCD0","ARMOUT","Edm_Dmriesg_Vw","EDW_Estadistica_Tb","Edw_Epiphany_Vw","Mkt_Crm_Adts_Tb","ARMTMI","Usr_Mkt_Common","Edw_OnBoarding_Vw","Edw_DMCBolsa_vw","Edm_Mis_Vw","RECUPERACION","EDW_VRISK","Bci_Qrylogv","ebastia","Ebi_PlMtc_Tb","ARMWORK","Edm_DmPrspct_Vw","eolavar","Mkt_Riesgo_Tb","PDCRDATA","tva_auth_v","LockLogShredder","dbcmngr6","bcidemo_vm","Edm_DmInvers_TB","csanche","EDW_Cross_Subject_Areas_Tb","Edm_DmDotacion_Tb","Edm_DmFinanc_Vw","usrdlake","Edc_SurveyBuy_Vw","EDW_Wrk","Performance","Mkt_SAPGestion_Tb","BCISF","reparacion","iriverm","exilago","dbcmngr","Edw_Governance_Vw","EDW_DmMkt_Vw","Edw_TareaSolicitud_Tb","EDW_Party_Tb","Mkt_Analytics_Per_Tb","Tva_Staging","Edw_Event_Fil_Tb","EDW_Channel_Tb","DS_EPUSTG","Mkt_Treasury_Tb","Edm_DmPrspct_Tb","rcardet","ARMCUST","ARMEDW","Edw_TempUsu","Edw_Matrix_Tb","Mkt_Analytics_Tb","tva_archive_v","BCISSFF","DataLake_Vw","Mkt_Tmp_Dtrsch_Tb","Customer_Service","Edw_Event_Trj_Tb","Edm_Tmp_Dmfc","EDW_Product_Tb","tva_dyn_sql","EDW_Agreement_Tb","Edm_DmEmpresa_Vw","Edm_DmActCom_Vw","Nagios","Edm_DmCore_Vw","Edw_Event_Nova_Tb","Edw_DMTarjeta_Vw","DS_ARMINP","EDW_Finance_Tb","DM_CONTROL","Edw_Vw_Pre_Gov","cpenap","Edw_BciWork_Tb","BCIADMIN","mmonsac","Mkt_Tmp_Input_Tb","tdwm","Edm_DmCto_Tb","Edw_OnBoarding_Tb","Edm_DmNova_Vw","Edm_DmFinanc_Tb","lmenesv","Edm_Mis_Tb","Edm_DmNova_Tb","Edw_Prospect_Tb","Edc_Sii_Vw","Edw_SemLab_Vw","SYSLIB","BCIWORK","EDW_Event_Tb","tva_amort","Edw_DMCBolsa_Tb","DBC","SystemFe","EDW_Campaign_Tb","Edw_DMRiesgo_Tb","Edw_DM_BBEE_Vw","BCIGESTION","Edw_Calidad_Vw","Mkt_CtrSrvCCSS_Tb","fancahu","Edc_Sii_Tb","Edw_DMRiesgo_Vw","BCIPROD","PDCRINFO","MKT_PRODUCTOS_TB","bo_bci","Edm_DmInvers_Vw","UAnalyCi","tva_auth","Edm_DmMtrcs_Vw","Edc_Mdd_Tb","Edm_Dmriesg_Tb","Edc_Mach_Tb","Mkt_Pryser_Tb","ARMINP","SysAdmin","Edc_Mdd_Vw","TDQCD","Edm_DmBdr_Vw","EPUCORE","DS_ARMOUT","BCIRISK","rearce","EDW_Party_Asset_Tb","Edc_CnlDigital_Tb","usrgtcmp","PR_EPUBCK","Mkt_Crm_Analytics_Tb","Ebi_PlByP_Tb","Mkt_Explorer_Tb","Sys_Calendar","Edw_DMbbp_Vw","tva_rules","EDW_Vw","sgiglio","Edw_BciDaily_Tb","EDW_Int_Organization_Tb","DS_ARMVIEWS","Edm_DmCore_Tb","mjinala","Edw_SporadicBuy_Tb","BCIMKT","EDW_DM_BBEE_Tb","Edc_Journey_Vw","BCISPEC","Capacity_Planning","Edm_DmMtrcs_TB"] # "tempusu", "TEMP" se elimino

def desparametrizador_airflow(file_path, version = ""):
    """
        Funcion para eliminar la parametrizacion de fechas de airflow
        idealmente para correr manualmente

        version : agrega version diferente al archivo original
        Autor: Bruno Gomez
    """
    sql_file = open(file_path, 'r')
    sql_as_string = sql_file.read()
    sql_as_string = sql_as_string.lower()

    file = sql_as_string
    file = file.replace("_{{ds_nodash}}_", "_")
    file = file.replace("cast('{{ ds }}' as date) + interval '1' month", "current_date")

    name_final = file_path[0:(len(file_path)-4)] + version + file_path[(len(file_path)-4):len(file_path)]
    new_file = open(name_final, "w+")
    new_file.write(file)


def parametrizador_airflow(file_path, version = ""):
    """
        Funcion para parametrizar tablas temporales y current_date para correr en airflow
        idealmente para dejar codigo y correrlo en airflow

        version : agrega version diferente al archivo original
        Autor: Bruno Gomez
    """
    sql_file = open(file_path, 'r')
    sql_as_string = sql_file.read()
    sql_as_string = sql_as_string.lower()

    file = sql_as_string
    file = file.replace("edw_tempusu.t_", "edw_tempusu.t_{{ds_nodash}}_")
    file = file.replace("edw_tempusu.p_", "edw_tempusu.p_{{ds_nodash}}_")
    file = file.replace("current_date", "cast('{{ ds }}' as date) + interval '1' month")

    name_final = file_path[0:(len(file_path)-4)] + version + file_path[(len(file_path)-4):len(file_path)]
    new_file = open(name_final, "w+")
    new_file.write(file)

def to_multiset(file_path, version = ""):
    """
        Funcion para crear tablas en formato multiset
        Autor: Bruno Gomez
    """
    sql_file = open(file_path, 'r')
    sql_as_string = sql_file.read()
    sql_as_string = sql_as_string.lower()

    file = sql_as_string
    file = file.replace("create table", "create multiset table")
    name_final = file_path[0:(len(file_path)-4)] + version + file_path[(len(file_path)-4):len(file_path)]
    new_file = open(name_final, "w+")
    new_file.write(file)

def find_all_spaces(a_str):
    """
        Funcion para encontrar todas las posiciones de espacio en un string
        Autor: Bruno Gomez
    """
    a_str = sql_as_string
    sub = r'\t+|\n+|\S+'

    spaces = re.finditer(sub,a_str)
    tuple_list = [(m.start(0), m.end(0)) for m in spaces]

    t_tuple_elements = [a_tuple[1] for a_tuple in tuple_list]

    return t_tuple_elements

def find_all(a_str, sub):
    """
        Funcion para encontrar todas las posiciones de un string en
        Autor: Bruno Gomez
    """
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub) # use start += 1 to find overlapping matches

def all_db_particular_in_sql(sql_as_string, squema):
    """
        PD: ACTUALIZAR COPIANDO MEJORAS DE all_db_in_sql
        Encuentra todas las bases de datos asociados a un esquema
        seleccionando desde el inicio de esquema hasta el espacio siguiente
        para encontrar las bases
        Autor: Bruno Gomez
    """
    sql_as_string = sql_as_string.lower()
    all_position_point = list(find_all(sql_as_string, squema))
    all_position_space = list(find_all(sql_as_string, ' '))

    databases = []
    for i_point in all_position_point:
        position_point = i_point
        distancias = all_position_space - np.array(position_point)
        distancias_neg = distancias[distancias < 0]
        l_space = distancias_neg.shape[0]-1
        r_space = distancias_neg.shape[0]

        l_str_space = all_position_space[l_space]
        r_str_space = all_position_space[r_space]

        if (abs(r_str_space-position_point) > 3):
            var_string = sql_as_string[l_str_space : r_str_space]
            databases.append(var_string.lower().strip())
    unique_db = set(databases)

    return(unique_db)

def all_db_in_sql(sql_as_string):
    """
        v3
        Encuentra todas las bases de datos contenidas en un codigo de sql que no de un esquema tempusu
        usa una lisa de todos los esquemas de teradata existentes (bases y vistas)
        Autor: Bruno Gomez
    """
    sql_as_string = sql_as_string.lower()
    lista_esquemas_lower = [x.lower() for x in lista_esquemas]

    # Buscamos el inicio de cada esquema en el string
    all_position_squema = []
    for squema in lista_esquemas_lower:
        all_position_squema = all_position_squema + list(find_all(sql_as_string, squema))

    # Buscamos los espacios del codigo
    # -- esto es para decirle al codigo anda desde el inicio de un esquema hasta algun espacios
    # -- correspondiente justamente al esque.tabla
    all_position_space = find_all_spaces(sql_as_string)

    databases = []
    for i_point in all_position_squema: # para cada inicio del codigo
        position_point = i_point
        distancias = all_position_space - np.array(position_point) # calculamos distancias entre esquema y espacios
        distancias_neg = distancias[distancias > 0] # nos quedamos solamente con distancias positivas
        r_space = distancias_neg[0] # tomamos la distancia mas cercana
        var_string = sql_as_string[position_point : position_point+r_space] # seleccionamos la seccion del string
        var_string = re.sub(r";", "", var_string) # se agregan algunos ; los eliminamos para luego hacer el set()
        var_string = re.sub("\)", "", var_string) # se agregan algunos ) los eliminamos para luego hacer el set()
        databases.append(var_string.lower().strip())

    unique_db = set(databases) # dejamos un unique de tablas por si hay duplicados

    return(unique_db)

def db_temporales_t(sql_as_string):
    """
        Funcion para identificar tablas temporales con prefijo .T_
        Autor: Bruno Gomez
    """
    db_pot = sql_as_string.split(';')
    db_creadas = [x for x in db_pot if 'create' in x]
    db_creadas = ' \n '.join(db_creadas)
    prueba = all_db_in_sql(db_creadas)
    db_temporales = [x for x in prueba if 'edw_tempusu.t' in x]

    return(db_temporales)

def db_temporales_p(sql_as_string, formato = "texto"):
    """
        Funcion para identificar tablas temporales con prefijo .P_
        que se crean en ese codigo, eliminando las tablas de entrada que son
        temporales y de precalculo

        formato = {texto, lista}
        Autor: Bruno Gomez
    """
    db_pot = sql_as_string.split(';') # Particionamos el codigo por query
    db_creadas = [x for x in db_pot if 'create' in x] # seleccionamos solo las queries del tipo CREATE
    db_creadas = ' \n '.join(db_creadas) # Juntamnos todas las queries
    prueba = all_db_in_sql(db_creadas) # Buscamos todas las tablas con funcion auxiliar
    db_precalculo = [x for x in prueba if 'edw_tempusu.p' in x] # Seleccionamos solo las tempusu que empiezan con P
    db_precalculo = sorted(db_precalculo) # Ordenamos por orden alfabetico

    # Paramos a construir una lista con las temporales
    pieza_texto = []
    for i in db_precalculo:
        pieza_texto.append("**        " + str(i) +"             **")
    db_temporales_p = '\n'.join([str(n) for n in pieza_texto])

    if formato == "texto":
        result = db_temporales_p
    elif formato == "lista":
        result = db_precalculo

    return(result)

def db_salida(sql_as_string, formato = "texto"):
    """
        Funcion para generar todas las tablas de salida en el encabezado
        formato = {texto, lista}
        Autor: Bruno Gomez
    """
    # Recuperamos las tablas que de precalculo que se crean en la BTEQ
    lista_temporales = db_temporales_p(sql_as_string, formato = "lista")

    # Buscamos tablas insert en productivo o desarrollo
    all_inserts = re.findall("insert into (.*)", sql_as_string)
    db_per_tb = [x for x in all_inserts if 'mkt_analytics_per_tb' in x] # inserts a tablas en desarrollo
    db_crm_tb = [x for x in all_inserts if 'mkt_crm_analytics_tb' in x] # inserts a tablas productivas

    lista_salida = lista_temporales + db_per_tb + db_crm_tb

    # Paramos a construir una lista con las temporales
    pieza_texto = []
    for i in lista_salida:
        pieza_texto.append("**        " + str(i) +"             **")
    db_lista_salida = '\n'.join([str(n) for n in pieza_texto])

    if formato == "texto":
        result = lista_salida
    elif formato == "lista":
        result = lista_salida

    return(result)


def db_entrada(sql_as_string):
    """
        Identifica todas las tablas de entrada que no son temporales
        Autor: Bruno Gomez
    """
    all_db =  all_db_in_sql(sql_as_string)
    all_temporal_t = db_temporales_t(sql_as_string)
    all_temporal_p = db_temporales_p(sql_as_string, "lista")

    db_entrada = set(all_db) - set(all_temporal_t)
    db_entrada = set(db_entrada) - set(all_temporal_p)
    db_entrada = sorted(db_entrada)

    pieza_texto = []
    for i in db_entrada:
        pieza_texto.append("**        " + str(i) +"             **")
    db_entradas = '\n'.join([str(n) for n in pieza_texto])

    return(db_entradas)

####################################
# Sección 2: Funciones de chequeo
####################################
def check_names_tempusu(sql_as_string):
    """
        Chequea si todos las temporales del codigo tienen prefijo T,P
        (quizas conviene crear una version query)
        Autor: Bruno Gomez
    """
    db_pot = sql_as_string.split(';')
    db_creadas = [x for x in db_pot if 'CREATE' in x]
    db_creadas = ' \n '.join(db_creadas)
    prueba = all_db_in_sql(db_creadas)
    db_tempusu = [x for x in prueba if 'edw_tempusu.' in x]
    db_tempusu = [x for x in db_tempusu if 'edw_tempusu.p' not in x]
    db_tempusu = [x for x in db_tempusu if 'edw_tempusu.t' not in x]
    db_tempusu = sorted(db_tempusu)

    return(db_tempusu)

def check_par_create_insert(sql_as_string):
    """
        Chequea si todas las tablas temporales son create insert
        EN DESARROLLO
        Autor: Bruno Gomez
    """
    db_pot = sql_as_string.split(';')
    db_creadas = [x for x in db_pot if 'create' in x]
    db_insertadas = [x for x in db_pot if 'insert' in x]

    return(db_tempusu)

def check_nombre_variables(sql_as_string):
    """
        Chequea si todas las variables tienen los nombres en formatOPS
        (por desarrollar)
        Autor: Bruno Gomez
    """
    db_pot = sql_as_string.split(';')
    db_creadas = [x for x in db_pot if 'create' in x]
    db_insertadas = [x for x in db_pot if 'insert' in x]

    return(db_tempusu)

def check_db_bcimkt(sql_as_string):
    """
        Funcion para encontrar uso de DB BCIMKT, será eliminada
    Autor: Bruno Gomez
    """
    return list(find_all(sql_as_string, 'bcimkt.'))

def check_create_table_as(sql_as_string):
    """
        Funcion para encontrar uso de CREATE TABLE AS
        Autor: Bruno Gomez
    """
    reg_exp = r'create.+table.+as'
    return re.findall(reg_exp, sql_as_string)


####################################
# Sección 3: Funciones de codigo completo
####################################

def add_encabezado(path_archivo,
                   version = "",
                   var_desc = "Descripcion del proceso",
                   var_autor = "Usuario corto o nombre",
                   var_empresa = "BCI",
                   var_fecha = "Fecha origen",
                   var_mantencion = "Descripcion de mantancion",
                   var_autor_mant = "Usuario corto o nombre",
                   var_fecha_mant = ""):
    """
        Funcion que entrega el encabezado con las tablas de entrada
        y las tablas de salida asociadas al codigo
        Autor: Bruno Gomez
    """
    sql_file = open(path_archivo, 'r')
    sql_as_string = sql_file.read()
    sql_as_string = sql_as_string.lower()

    var_fecha = date.today().strftime('%Y/%m/%d')
    var_fecha_mant = date.today().strftime('%Y/%m/%d')

    p1 = """/*********************************************************************
**********************************************************************
** DESCRIPCION: var_desc                                            **
** AUTOR  : var_autor                                               **
** EMPRESA: var_empresa                                             **
** FECHA  : var_fecha                                               **
*********************************************************************/
/*********************************************************************
** MANTENCION: var_mantencion                                       **
** AUTOR  : var_autor_mant                                          **
** FECHA  : var_fecha                                               **
/*********************************************************************
** TABLA DE ENTRADA :                                               ** """
    p2 = str(db_entrada(sql_as_string))
    p3 = """** TABLA DE SALIDA  :                                               **"""
    p4 = str(db_salida(sql_as_string))
    p5 = """ **********************************************************************
*********************************************************************/\n"""

    p1 = p1.replace("var_desc", var_desc)
    p1 = p1.replace("var_autor", var_autor)
    p1 = p1.replace("var_empresa", var_empresa)
    p1 = p1.replace("var_fecha", var_fecha)
    p1 = p1.replace("var_mantencion", var_mantencion)
    p1 = p1.replace("var_autor_mant", var_autor_mant)
    p1 = p1.replace("var_fecha", var_fecha_mant)

    encabezado_previo = [p1, p2, p3, p4, p5]
    encabezado = '\n'.join([str(n) for n in encabezado_previo])

    btq_final = encabezado + sql_as_string

    name_final = path_archivo[0:(len(path_archivo)-4)] + version + path_archivo[(len(path_archivo)-4):len(path_archivo)]
    new_file = open(name_final, "w+")
    new_file.write(btq_final)

    return("encabezado agregado")

def add_drop_temporales(path_archivo, version = ""):
    """
        Funcion que agrega todos los drop table de tablas temporales
        Autor: Bruno Gomez
    """
    sql_file = open(path_archivo, 'r')
    sql_as_string = sql_file.read()
    sql_as_string = sql_as_string.lower()

    lista_temporales = db_temporales_t(sql_as_string)

    cabecera = """
/* ***********************************************************************/
/*                  ELIMINAMOS TABLAS TEMPORALES                        */
/* **********************************************************************/
    """

    pieza_texto = [cabecera]
    for i in lista_temporales:
        pieza_texto.append("DROP TABLE " + str(i) +";")

    txt_temporales = '\n'.join([str(n) for n in pieza_texto])

    if not lista_temporales:
        txt_temporales = "-- No hay tablas temporales en la query"

    btq_final = sql_as_string + "\n\n" + txt_temporales

    name_final = path_archivo[0:(len(path_archivo)-4)] + version + path_archivo[(len(path_archivo)-4):len(path_archivo)]
    new_file = open(name_final, "w+")
    new_file.write(btq_final)

    return("print(txt_temporales)")

def add_iferrors(path_archivo, version = ""):
    """
        Funcion que devuelve un archivo con todos los iferrors
        de forma enumerada

        Problemas:
        - hay que eliminar los comentarios del codigo antes de todo,
          porque si hay un create comentado igual agrega un iferror
      Autor: Bruno Gomez
    """
    sql_file = open(path_archivo, 'r')
    sql_as_string = sql_file.read()
    sql_as_string = sql_as_string.lower()
    sqlCommands = sql_as_string.split(';')
    print(len(sqlCommands))

    db_tempusu = [x for x in sqlCommands if 'if error' not in x]
    position = 0
    number_error = 0
    lst_errores = []
    lst_position = []
    # if query.find("create") > 0 or query.find("insert") > 0 or query.find("delete") > 0 or query.find("collect"):

    for query in db_tempusu:
        if query.find("create") > 0 or query.find("insert") > 0 or query.find("del from") > 0 or query.find("delete") > 0 or query.find("collect") > 0:
            number_error += 1
            error = "\n.IF ERRORCODE <> 0 THEN .QUIT " + str(number_error) + ""
            lst_errores.append(error)
            lst_position.append(position)
        position += 1

    final = []
    n_error = 0
    for index in range(0,len(db_tempusu)):
        final.append(db_tempusu[index])
        if index in lst_position:
            final.append(lst_errores[n_error])
            n_error += 1

    name_final = path_archivo[0:(len(path_archivo)-4)] + version + path_archivo[(len(path_archivo)-4):len(path_archivo)]
    txt_final = ';'.join([str(n) for n in final])
    new_file = open(name_final, "w+")
    new_file.write(txt_final)
    return("if error agregados")

def add_collects(path_archivo, version = ""):
    """
        Funcion para agregar los collects al codigo
        Autor: Bruno Gomez
    """
    sql_file = open(path_archivo, 'r')
    sql_as_string = sql_file.read()
    sql_as_string = sql_as_string.lower()
    sqlCommands = sql_as_string.split(';')
    print(len(sqlCommands))

    queries_sin_collect = [x for x in sqlCommands if 'collect' not in x]
    position = 0
    number_error = 0
    lst_collects = []
    lst_position = []

    sql_as_string = sql_as_string.lower()
    lista_esquemas_lower = [x.lower() for x in lista_esquemas]

    add_line = ""
    lst_collects = []
    for query in queries_sin_collect:
        if query.find("create") > 0:

            # Recuperamos el nombre de la tabla
            db_position = query.find("edw_tempusu")
            all_position_space = list(find_all(query, ' '))
            index_lista = [ n for n,i in enumerate(all_position_space) if i>db_position ][0]
            db_fin = all_position_space[index_lista]
            var_tabla = query[db_position:db_fin]

            # Recuperamos las variables index
            query_v2 = query.replace(" ", "")
            alt1 = query_v2.find("index(")

            find_parantesis = list(find_all(query_v2, ")"))
            find_cierre_query = find_parantesis[len(find_parantesis)-1]

            var_index = query_v2[(alt1+6): (find_cierre_query) ]

            # Creamos el collect
            if var_index.find(",") > 0:
                add_line = "\ncollect stats index (" + var_index +  ") \n   on " + var_tabla
            else:
                add_line = "\ncollect stats index (" + var_index +  ") \n   on " + var_tabla

            position += 1
        elif (query.find("insert") > 0) and (add_line != ""):
            lst_collects.append(add_line)
            lst_position.append(position)
            position += 1
        else:
            position += 1

    final = []
    n_collect = 0
    for index in range(0,len(queries_sin_collect)):
        final.append(queries_sin_collect[index])
        if index in lst_position:
            final.append(lst_collects[n_collect])
            n_collect += 1

    name_final = path_archivo[0:(len(path_archivo)-4)] + version + path_archivo[(len(path_archivo)-4):len(path_archivo)]
    txt_final = ';'.join([str(n) for n in final])
    new_file = open(name_final, "w+")
    new_file.write(txt_final)
    return("collects agregados")


def add_inicio_fin(path_archivo, version = ""):
    """
        Funcion para agregar formato de inicio y fin
        Autor: Bruno Gomez
    """
    sql_file = open(path_archivo, 'r')
    sql_as_string = sql_file.read()
    sql_as_string = sql_as_string.lower()
    sqlCommands = sql_as_string.split(';')

    queries = [x for x in sqlCommands if ".set session charset 'utf8'" not in x]
    queries = [x for x in queries if '.set session charset "utf8"' not in x]
    queries = [x for x in queries if 'select date, time' not in x]
    queries = [x for x in queries if '.quit 0' not in x]

    queries = ['\nselect date, time'] + queries
    queries = ["\n.set session charset 'utf8'"] + queries


    queries.append('\nselect date, time')
    queries.append('\n.quit 0;')

    try:
        queries.remove('')
    except:
        pass

    name_final = path_archivo[0:(len(path_archivo)-4)] + version + path_archivo[(len(path_archivo)-4):len(path_archivo)]
    txt_final = ';'.join([str(n) for n in queries])
    new_file = open(name_final, "w+")
    new_file.write(txt_final)
    return("agregada las sentencias de inicio y fin")

def ruta_sqls(bteq_path):
    """
        Funcion para extraer todos los path de archivos sql dentro de una carpeta BTEQ
        -- Dejar sin el "/" Final
        Autor: Bruno Gomez
    """
    folders_bteq = [x[0] for x in os.walk(bteq_path)]
    #print(folders_bteq)
    path_sqls = []
    for i in folders_bteq:
        lista_files = os.listdir(i)
        for file in lista_files:
            path_sqls.append(i + "/" + file)

    # Dejamos solamente los sql
    path_sqls_final = [x for x in path_sqls if '.sql' in x]
    path_sqls_final = [x for x in path_sqls_final if 'en_desarrollo' not in x]

    return(path_sqls_final)

def add_respaldo(path_activo_analitico):
        """
            Funcion para respaldar el activo antes de utilizar add_parametrops()
            -- Dejar sin el "/" Final
            Autor: Bruno Gomez
        """
        src = path_activo_analitico
        dest = path_activo_analitico + "_respaldo"

        if os.path.isdir(dest) == False:
            destination = shutil.copytree(src, dest)
            txt = "Activo analitico clonado"
        else:
            txt = "Activo analitico ya se encuentra respaldado"
        return(txt)

def add_parametrops(path_archivo, version = ""):
    """
        Funcion orquestadora de funciones
        Autor: Bruno Gomez
    """
    #Creamos el archivo en caso que no existe
    name_final = path_archivo[0:(len(path_archivo)-4)] + version + path_archivo[(len(path_archivo)-4):len(path_archivo)]
    if os.path.isfile(name_final) == False:
        sql_file = open(path_archivo, 'r')
        sql_as_string = sql_file.read()
        sql_as_string = sql_as_string.lower()
        new_file = open(name_final, "w+")
        new_file.write(sql_as_string)

    add_iferrors(name_final)
    add_collects(name_final)
    add_drop_temporales(name_final)
    add_inicio_fin(name_final)
    add_encabezado(name_final)
    return("se agrega encabezado, drop temporales, iferrors, collects y parametros de inicio y fin query")

print("Funciones cargadas")
